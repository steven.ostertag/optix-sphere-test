file(REMOVE_RECURSE
  "../bin/optixDynamicGeometry"
  "../bin/optixDynamicGeometry.pdb"
  "../lib/ptx/optixDynamicGeometry_generated_vertices.cu.o"
  "CMakeFiles/optixDynamicGeometry.dir/optixDynamicGeometry.cpp.o"
  "CMakeFiles/optixDynamicGeometry.dir/optixDynamicGeometry.cpp.o.d"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/optixDynamicGeometry.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
