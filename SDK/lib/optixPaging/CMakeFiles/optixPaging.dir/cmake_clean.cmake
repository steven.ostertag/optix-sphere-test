file(REMOVE_RECURSE
  "../liboptixPaging.a"
  "../liboptixPaging.pdb"
  "../ptx/optixPaging_generated_optixPaging.cu.o"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/optixPaging.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
