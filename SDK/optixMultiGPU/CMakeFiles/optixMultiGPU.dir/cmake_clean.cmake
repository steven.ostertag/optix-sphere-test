file(REMOVE_RECURSE
  "../bin/optixMultiGPU"
  "../bin/optixMultiGPU.pdb"
  "../lib/ptx/optixMultiGPU_generated_optixMultiGPU_kernels.cu.o"
  "CMakeFiles/optixMultiGPU.dir/optixMultiGPU.cpp.o"
  "CMakeFiles/optixMultiGPU.dir/optixMultiGPU.cpp.o.d"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/optixMultiGPU.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
