# Install script for directory: /home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

# Set default install directory permissions.
if(NOT DEFINED CMAKE_OBJDUMP)
  set(CMAKE_OBJDUMP "/usr/bin/objdump")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixBoundValues/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixCallablePrograms/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixCurves/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixCutouts/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixDemandPaging/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixDemandTexture/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixDenoiser/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixDynamicGeometry/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixDynamicMaterials/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixHair/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixHello/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixMeshViewer/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixMultiGPU/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixNVLink/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixPathTracer/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixRaycasting/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixSimpleMotionBlur/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixSphere/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixTriangle/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/optixWhitted/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/sutil/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/lib/DemandLoading/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/lib/optixPaging/cmake_install.cmake")
  include("/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/support/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/steven.ostertag/Downloads/NVIDIA-OptiX-SDK-7.2.0-linux64-x86_64/SDK/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
