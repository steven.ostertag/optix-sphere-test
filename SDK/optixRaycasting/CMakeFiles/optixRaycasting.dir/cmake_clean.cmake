file(REMOVE_RECURSE
  "../bin/optixRaycasting"
  "../bin/optixRaycasting.pdb"
  "../lib/ptx/optixRaycasting_generated_optixRaycastingKernels.cu.o"
  "CMakeFiles/optixRaycasting.dir/optixRaycasting.cpp.o"
  "CMakeFiles/optixRaycasting.dir/optixRaycasting.cpp.o.d"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/optixRaycasting.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
